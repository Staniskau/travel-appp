//
//  MapViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/24/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerDidSelecPoint(_ point: MKPointAnnotation)
}

class MapViewController: UIViewController {
    //    MARK: - Properties
    
    var array:  [MKPointAnnotation]?
    var delegate: MapViewControllerDelegate?
    
    //MARK: - Outlet
    @IBOutlet weak var mapView: MKMapView!
    
    //    MARK: - Action
    
    @IBAction func saveClicked(_ sender: Any) {
        if let selectedPoint = mapView.selectedAnnotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelecPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    //    MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let array = array{
            for point in array {
                mapView.addAnnotation(point)
            }
        }
    }
}
