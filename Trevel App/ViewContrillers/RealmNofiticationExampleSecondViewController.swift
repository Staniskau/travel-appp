//
//  RealmNofiticationExampleSecondViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/5/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class RealmNofiticationExampleSecondViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    @IBAction func textFieldEditingChanded(_ sender: Any) {
        DatabaseManager.intance.updateTravel(travel!, withName: textField.text!)
    }
    
    
    var travel: RealmTravel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travel = DatabaseManager.intance.getTravelsFromDAtaBase(withId: "xxx")
        
    
    }
}


extension RealmNofiticationExampleSecondViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
    
    }
    
}
