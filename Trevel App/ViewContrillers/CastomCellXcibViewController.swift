//
//  CastomCellXcibViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/14/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class CastomCellXcibViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let xib = UINib.init(nibName: "CustomXibCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserAvatar didChanget"),
        object: nil,
        queue: nil, using: { (notification) in
            print("Случилось событие!!!! \(notification.object)")
            self.topLabel.text = notification.object as? String
        })
    }
    
}

extension CastomCellXcibViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    
}
