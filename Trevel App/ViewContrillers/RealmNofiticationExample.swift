//
//  RealmNofiticationExample.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/5/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import RealmSwift

class RealmNofiticationExample: UIViewController {

    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func buttonClicked(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travel = RealmTravel()
        travel?.id = "xxx"
        travel?.name = "gg"
        DatabaseManager.intance.saveToDatabase(travel: travel!)
        notificationToken = travel?.observe({ (change) in
            switch change {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                       self.resultLabel.text = property.newValue as? String
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
            }
        })
        
    }
}
