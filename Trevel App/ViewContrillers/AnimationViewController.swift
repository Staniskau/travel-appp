//
//  AnimationViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/7/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var secondSquare: UIView!
    @IBOutlet weak var firstSquare: UIView!
    @IBOutlet weak var secondSquareConstraint: NSLayoutConstraint!
    
    @IBAction func thirdButtonClicked(_ sender: Any) {
        let scale = CABasicAnimation(keyPath: "transform.scale")
        scale.duration = 0.5
        scale.fromValue = 1
        scale.toValue = 1.5
        scale.repeatCount = .infinity
        scale.autoreverses = true
        secondSquare.layer.add(scale, forKey: "ZZzzz")
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        secondSquareConstraint.constant = 300
        UIView.animate(withDuration: 0.3, animations: {
            self.secondSquare.frame.origin.y += 90
            self.view.layoutIfNeeded()
        })
    }
    
    
    
    @IBAction func buttonClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.firstSquare.frame.origin.y += 90
            self.firstSquare.layer.cornerRadius = self.firstSquare.frame.size.width / 2
            self.firstSquare.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.firstSquare.backgroundColor = .red
        }, completion: { success in
            
        })
        
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    
    
}
