//
//  CreateANAccountViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/19/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class CreateANAccountViewController: UIViewController {
  
    @IBOutlet weak var password: UITextField!
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func enterButton(_ sender: Any) {
        
    }
    
    
    @IBAction func hideButton(_ sender: Any) {
        password.isSecureTextEntry.toggle()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    
    
}
