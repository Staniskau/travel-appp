//
//  LoginViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/11/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var createeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    
    @IBAction func loginClicked(_ sender: Any) {
        let loginController = UIViewController.getFromStoryBoard(withId: "LoginWithEmailViewController")
        navigationController?.pushViewController(loginController!, animated: true)
        
    }
    
    @IBAction func registrClicked(_ sender: Any) {
        let creatController = UIViewController.getFromStoryBoard(withId: "CreateANAccountViewController")
        navigationController?.pushViewController(creatController!, animated: true)
    }
    
    
    
    @IBOutlet var allButton: [UIButton]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for buttonAll in allButton {
            buttonAll.layer.cornerRadius = 5
            navigationController?.isNavigationBarHidden = true
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
