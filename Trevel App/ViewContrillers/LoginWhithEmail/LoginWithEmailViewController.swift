//
//  LoginWithEmailViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/16/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class LoginWithEmailViewController: UIViewController {
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
//    var emailTextField: String = ""
//    var passwordTextField: String = ""
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var passwordSeperator: UIView!
    
    @IBAction func forGotPasswordClicked(_ sender: Any) {
        let forGotPasswordController = UIViewController.getFromStoryBoard(withId: "ForGotPasswordViewController")
        navigationController?.pushViewController(forGotPasswordController!, animated: true)
        
        
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        if emailTextField.text == "login" &&
            passwordTextField.text == "password" {
            let loginController = UIViewController.getFromStoryBoard(withId: "TripsViewController")
            navigationController?.pushViewController(loginController!, animated: true)
        } else {
           passwordLabel.textColor = .red
            passwordSeperator.backgroundColor = .red
        }
    }

        
        
        
        
        
        @IBAction func backClicked(_ sender: Any) {
            navigationController?.popViewController(animated: true)
        }
        
        
        
        
        @IBAction func hideButton(_ sender: Any) {
            password.isSecureTextEntry.toggle()
        }
        
        
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            loginButton.layer.cornerRadius = 5
            
        
        
    }
}
