//
//  TripsViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import RealmSwift

class TravelsListViewController: UIViewController {
    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var PlusTravelLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    // MARK: - Actions
    
    
    
    @IBAction func travelAddButton(_ sender: Any) {
        let alertController = UIAlertController(title: "Введите название страны", message: nil, preferredStyle: .alert)
        
        let createAction = UIAlertAction(title: "Создать", style: .default) { (_) in
            
            let countryTextField = alertController.textFields![0] as UITextField
            let descriptionTextField = alertController.textFields![1] as UITextField
            
//            let travel = Travel()
//            if let country = countryTextField.text {
//                travel.name = country
//            }
//            if let description = descriptionTextField.text {
//                travel.desc = description
//            }
            
            let realmTravel = RealmTravel()
            realmTravel.name = countryTextField.text ?? ""
            realmTravel.desc = descriptionTextField.text ?? ""
            DatabaseManager.intance.save([realmTravel])
//            DatabaseManager.intsnce.saveToDatabase(travel: realmTravel)
            
            self.travels.append(realmTravel)
            self.tableView.reloadData()
            
            
            if self.travels.isEmpty {
                self.PlusTravelLabel.isHidden = false}
            else {
                self.PlusTravelLabel.isHidden = true
            }
            
            
        }
        createAction.isEnabled = true
        alertController.addAction(createAction)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (_) in
            
        }
        alertController.addAction(cancelAction)
        
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Страна"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Описание"
            
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                createAction.isEnabled = textField.text != ""
            }
        }
        present(alertController, animated: true)
    }
    
    
    // MARK: - Properties
    var delegateStopList: StopsListViewController?
    var travels: [RealmTravel] = []
    var average: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        travels = DatabaseManager.intance.getObjects(classType: RealmTravel.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}
extension TravelsListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TravelCell
        let travel = travels[indexPath.row]
        cell.travelNameLabel.text = travel.name
        cell.travelSuptitleLabel.text = travel.desc
        cell.transtportView.layer.shadowColor = UIColor.black.cgColor
        cell.transtportView.layer.shadowOpacity = 1
        cell.transtportView.layer.shadowOffset = .zero
        cell.transtportView.layer.shadowRadius = 5
        cell.transtportView.layer.cornerRadius = 15
        
        let averrage = travel.getAverageRating()
        switch average {
        case 1:
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = false
            cell.starTree.isHighlighted = false
            cell.starFore.isHighlighted = false
            cell.starfive.isHighlighted = false
        case 2:
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starTree.isHighlighted = false
            cell.starFore.isHighlighted = false
            cell.starfive.isHighlighted = false
        case 3:
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starTree.isHighlighted = true
            cell.starFore.isHighlighted = false
            cell.starfive.isHighlighted = false
        case 4:
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starTree.isHighlighted = true
            cell.starFore.isHighlighted = true
            cell.starfive.isHighlighted = false
        case 5:
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starTree.isHighlighted = true
            cell.starFore.isHighlighted = true
            cell.starfive.isHighlighted = true
        default:
            print("ffff")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopVC = UIViewController.getFromStoryBoard(withId: "StopsListViewController") as! StopsListViewController
        stopVC.realmTravel = travels[indexPath.row]
        stopVC.delegate = self
        navigationController?.pushViewController(stopVC, animated: true)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            DatabaseManager.intance.deleteTravels(travels[indexPath.row])
            travels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TravelCell
    //        cell.travelNameLabel.text = "XXX"
    //        cell.travelSuptitleLabel.text = "yyy"
    //        return cell
    //    }
}

extension TravelsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            travels = DatabaseManager.intance.getTravelFromDatabase()
            tableView.reloadData()
            return
        }
        travels = travels.filter({ realmTravel -> Bool in
            realmTravel.name.lowercased().contains(searchText.lowercased())
            
        })
        tableView.reloadData()
    }
    
}
        
        


//func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
////        if editingStyle == UITableViewCell.EditingStyle.delete {
////             DataBaseManager.instance.removeTravel(travels[indexPath.row])
////            travels.remove(at: indexPath.row)
////            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
////            DataBaseManager.instance.removeTravel(travels[indexPath.row])
////        }
////    }
