//
//  UiViewController+CoreKti.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

extension UIViewController {
   static func getFromStoryBoard(withId id: String) -> UIViewController? {
        let storybord = UIStoryboard(name: "Main", bundle: nil)
        let controller = storybord.instantiateViewController(withIdentifier: id)
        return controller
    }
}
