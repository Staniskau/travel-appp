//
//  StopsListViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/27/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit


class StopsListViewController: UIViewController {
    
//    var delegate: CreateStopViewController?
    //MARK: - Outlets
    
    @IBOutlet weak var buttonClicked: UIBarButtonItem!
    
    @IBOutlet weak var buttonPlus: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var stopListLabel: UILabel!
    

    
    
    
    //MARK: - Action
    
    @IBAction func backButtonClicket(_ sender: Any) {

        let controllerFromStorybord = storyboard?.instantiateViewController(withIdentifier: "CreateStopViewController" )
        navigationController?.pushViewController(controllerFromStorybord!, animated: true)
    }
    
    @IBAction func ButtonClicket(_ sender: Any) {
        let controllerFromStorybord = storyboard?.instantiateViewController(withIdentifier: "CreateStopViewController")
        if let labelVC = controllerFromStorybord as? CreateStopViewController {
            labelVC.delegate = self
            labelVC.stopDidCreateClosure = { stop in
                DatabaseManager.intance.addStop(stop, to: self.realmTravel)
                DatabaseManager.intance.saveToDatabase(travel: self.realmTravel)
                
            }
            navigationController?.pushViewController(controllerFromStorybord!, animated: true)
        }
    }
    
//    @IBAction func BackButtonClicked(_ sender: Any) {
////        if count != 0 {
////        count = arrayStopsRating.count
////        for index in arrayStopsRating {
////            sumArrayStopsRank += index
////        }
////        delegate?.average = sumArrayStopsRank/count
//        delegate?.tableView.reloadData()
//        navigationController?.popViewController(animated: true)
//        } else {
//            navigationController?.popViewController(animated: true)
//        }
//
//    }
    
    //    MARK: - Propertis
//    var stops: [Stop] = []
    var transport: [RealmStop] = []
    var realmTravel: RealmTravel!
    var stops: [RealmStop] = []
    
//    var test:Int?
//    if(test != nil) {
//    print(test!) // развернутое значение
//    } else {
//    print(test) // nil
//    }
//    var arrayStopsRating: [Int] = []
//    var sumArrayStopsRank = 0
//    var count = 0
    var delegate: TravelsListViewController?
    //    MARK: - Lifeticle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        viewStops.layer.cornerRadius = 15
//        viewTransport.layer.cornerRadius = 15
    
        
       
        //Detault Background clear tableView.backgroundColor = UIColor.clear
//        tableView.delegate = self as! UITableViewDelegate
//        tableView.dataSource = self
//        tableView.separatorStyle = .none
     
        
      
        
//       buttonClicked.layer.borderWidth = 2
        
        //        let stop1:Stop = Stop()
        //        stop1.name = "Ирландия"
        //        stop1.desc = "Танос пыль"
        //
        //        stops.append(stop1)
        //
        //        let stop2:Stop = Stop()
        //        stop2.name = "Африка"
        //        stop2.desc = "ляпис"
        //        stops.append(stop2)
        //
        //        tableView.reloadData()
        
        
        //    }
        //    func name (_ text: String) {
        //        nameLabel.text = text
        //    }
        //    func rating (_ text: String){
        //        ratingLabel.text = text
        //    }
        //    func geolocation (_ text: String){
        //        geolocationLabel.text = text
        //    }
        //    func cush (_ text: String){
        //        cashLabel.text = text
        //    }
        //    func transport (_ text: String){
        //        transportLabel.text = text
        //    }
        //    func discrip (_ text: String){
        //        discripLabel.text = text
        //    }
        
        //    func didCreateStop(_ stop: Stop) {
        //        nameLabel.text = stop.name
        //        ratingLabel.text = text
        //        geolocationLabel.text = text
        //        cashLabel.text = text
        //        transportLabel.text = text
        //        discripLabel.text = text
        //    }
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        tableView.reloadData()
    }
    //    почти показали и есть верстка
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        viewStops.layer.cornerRadius = 10
//    }
}


extension StopsListViewController : UITabBarDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if realmTravel.stops.count == 0 {
            stopListLabel.isHidden = false
        } else {
            stopListLabel.isHidden = true
        }
        return realmTravel.stops.count
    }


//            viewStops.layer.cornerRadius = 5
//            viewTransport.layer.cornerRadius = 5
 
//        func transpotView(_ transportView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableView {
//            let cell1 = transportView.dequeueReusableCell(withIdentifier: "transportCell", for: indexPath) as! TransportCell
//            let transport = stops[indexPath.row]
//            return cell1
//        }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath) as! StopCell
        let stop = realmTravel.stops[indexPath.row]
        cell.stopTitleLabel.text = stop.name
        cell.stopSubtitleLabel.text = stop.desc
        cell.stopSpendMoney.text = stop.spendMoney
        for stopCollection in cell.stopsCollection {
            stopCollection.layer.shadowColor = UIColor.black.cgColor
            stopCollection.layer.shadowColor = UIColor.black.cgColor
            stopCollection.layer.shadowOpacity = 1
            stopCollection.layer.shadowOffset = .zero
            stopCollection.layer.shadowRadius = 5
            stopCollection.layer.cornerRadius = 15
            
        }
//        cell.viewStops.layer.shadowColor = UIColor.black.cgColor
//        cell.viewStops.layer.shadowOpacity = 1
//        cell.viewStops.layer.shadowOffset = .zero
//        cell.viewStops.layer.shadowRadius = 5
//        cell.viewStops.layer.cornerRadius = 15
//        cell.transportView.layer.shadowColor = UIColor.black.cgColor
//        cell.transportView.layer.shadowOpacity = 1
//        cell.transportView.layer.shadowOffset = .zero
//        cell.transportView.layer.shadowRadius = 5
//        cell.transportView.layer.cornerRadius = 15
        
        //
        //            cell.stopGeolocdtionLabel.text = stop.geolocation
//        cell.stopTransportType.text = stop.transportType.map { $0.rawValue }
//        

        
//        if let transportImege = stop.transportType {
//           if stop.transportType == .airplane {
//            cell.AirplaneImage.isHidden = false
//            cell.trainImage.isHidden = true
//            cell.carImage.isHidden = true
//           }
//            if stop.transportType == .train {
//                cell.AirplaneImage.isHidden = true
//                cell.trainImage.isHidden = false
//                cell.carImage.isHidden = true
//            }
//            if stop.transportType == .car {
//                cell.AirplaneImage.isHidden = true
//                cell.trainImage.isHidden = true
//                cell.carImage.isHidden = false
//            }
//
//        }
        
       
        
        if stop.rating == 1 {
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = false
            cell.starThree.isHighlighted = false
            cell.starFoer.isHighlighted = false
            cell.starFive.isHighlighted = false
          
        }
        if stop.rating == 2 {
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starThree.isHighlighted = false
            cell.starFoer.isHighlighted = false
            cell.starFive.isHighlighted = false
     
        }
        if stop.rating == 3 {
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starThree.isHighlighted = true
            cell.starFoer.isHighlighted = false
            cell.starFive.isHighlighted = false
           
        }
        if stop.rating == 4 {
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starThree.isHighlighted = true
            cell.starFoer.isHighlighted = true
            cell.starFive.isHighlighted = false
        
        }
        if stop.rating == 5 {
            cell.starOne.isHighlighted = true
            cell.starTwo.isHighlighted = true
            cell.starThree.isHighlighted = true
            cell.starFoer.isHighlighted = true
            cell.starFive.isHighlighted = true
         
        }
//        cell.viewStops.layer.cornerRadius = 10
//        cell.viewTransports.layer.cornerRadius = 10
        //        viewStops.layer.cornerRadius = 5
        //        viewTransport.layer.cornerRadius = 5
//                cell.travelNameLabel.text = "XXX"
//                cell.travelSuptitleLabel.text = "yyy"
        return cell
    }
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == UITableViewCell.EditingStyle.delete {
//            DatabaseManager.intsnce.deleteStop(stops[indexPath.row])
//            stops.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
//            tableView.beginUpdates()
//            tableView.endUpdates()
//        }
//    }

}


extension StopsListViewController: CreateStopViewControllerDelegate {
    func creatStopControllerDidCreateStop(_ stop: RealmStop) {
        
        realmTravel.stops.append(stop)
        tableView.reloadData()
    }
}

