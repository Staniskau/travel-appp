//
//  UresListViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class UresListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
//    var textUsers: [String] = []
    var users: [RealmsUser] = []
    
//    var adress: [Adress] = []
//    var companu: [Cpmpany] = []
//    экран показан но без верстки
    override func viewDidLoad() {
        super.viewDidLoad()
        users = DatabaseManager.intance.getUsersFromDatabase()
//        ApiManager.instance.getUsers { (userFromServer) in
//            for user in userFromServer{
//                let realmUser = RealmsUser()
//                realmUser.id = String(user.id)
//                realmUser.name = user.name ?? "no data"
//                realmUser.email = user.email ?? "no data"
//                realmUser.username = user.username ?? "no data"
//                realmUser.phone = user.phone ?? "no data"
//                realmUser.website = user.website ?? "no data"
//                realmUser.suite = user.adress?.suite ?? "no data"
//                realmUser.street = user.adress?.street ?? "no data"
//                realmUser.city = user.adress?.city ?? "no data"
//                realmUser.zipcode = user.adress?.zipcode ?? "no data"
//                realmUser.nameCompanu = user.company?.name ?? "no data"
//                realmUser.catchPhrase = user.company?.catchPhrase ?? "no data"
//                realmUser.bs = user.company?.bs ?? "no data"
//                realmUser.longitude = user.adress?.geo?.longitude ?? "no data"
//                realmUser.latitude = user.adress?.geo?.latitude ?? "no data"
//                self.users.append(realmUser)
//            }
//            DatabaseManager.intsnce.saveToDatabas(users: self.users)
////            DatabaseManager.instance.save
////            self.users = userFromServer
            self.tableView.reloadData()
//        }
    
        tableView.delegate = self
        tableView.dataSource = self
    }
//    почти показали
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
//        if let text = textField.text {
//            test2Controller.text = text
//        }
//        //            test2Controller.delegate = self
//        test2Controller.closure = { text in
//            self.textLabel2.text = text
//        }
    }
    //    почти показали и есть верстка
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
//    срабатывает за долю секунды дл закрытия
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
//    срабатывает сразу после того как экран исчез
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    deinit {
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
//    когда происходит перелимит оперативной памати
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension UresListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row]
//        let address = adress[indexPath.row]
//        let companis = companu[indexPath.row]
        cell.idLabel.text = String(user.id)
        cell.nameLabel.text = user.name
        cell.userNameLabel.text = user.username
        cell.emailLabel.text = user.email
        cell.phoneLabel.text = user.phone
        cell.websiteLabel.text = user.website
        cell.streetLabel.text = user.street
        cell.suiteLabel.text = user.suite
        cell.cityLabel.text = user.city
        cell.zipcodeLabel.text = user.zipcode
        cell.nameCompanyLabel.text = user.name
        cell.bsLabel.text = user.bs
        cell.catchPhraseLabel.text = user.catchPhrase
        cell.latitudeLabel.text = user.latitude
        cell.longitudeLabel.text = user.longitude
        
        
//        if let text = textField.text {
//            test2Controller.text = text
//        }
//        //            test2Controller.delegate = self
//        test2Controller.closure = { text in
//            self.textLabel2.text = text
//        }
//        }
//        cell.nameLabel.text = user.name
//        cell.idLabel.text = String(user.id)
//        cell.userNameLabel.text = user.username
//        cell.emailLabel.text = user.email
//        cell.phoneLabel.text = user.phone
//        cell.websiteLabel.text = user.website
////        cell.streetLabel.text = address.street
//        cell.streetLabel.text = address.city
//        cell.zipcodeLabel.text = address.zipcode
//        cell.nameCompanyLabel.text = companis.name
//        cell.bsLabel.text = companis.bs
       
        return cell
    }
}
