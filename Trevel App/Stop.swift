//
//  stop.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/19/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class Stop {
    var name: String = ""
    var rating: String = ""
    var geolocation: Geolocation?
    var spendMoney: String = ""
    var transportType: TransportType?
    var desc: String = ""
    
    func printAll() {
        print("""
            Название - \(name)
            Рейтинг - \(rating)
            Потратил - \(spendMoney)
            Транспорт - \(transportType)
            Описание - \(desc)
            """)
    }
}
