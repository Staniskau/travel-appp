//
//  RealmTravelsFile.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/31/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import  RealmSwift

class RealmTravel: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name:String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var rating: String = ""
    let stops = List<RealmStop>()
    
    func getAverageRating () -> Int {
        if stops.count <= 0 {
            return 0
        }
        var summ: Int = 0
        for stop in stops{
            summ = summ + stop.rating
        }
        return summ/stops.count
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
