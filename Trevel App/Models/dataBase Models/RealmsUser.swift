//
//  RealmsUsers.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/29/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//


import UIKit
import RealmSwift

class RealmsUser: Object {
    @objc dynamic var id:  String = ""
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var username: String?
    @objc dynamic var phone: String?
    @objc dynamic var website: String?
    @objc dynamic var suite: String?
    @objc dynamic var city: String?
    @objc dynamic var street: String?
    @objc dynamic var zipcode: String?
    @objc dynamic var nameCompanu: String?
    @objc dynamic var catchPhrase: String?
    @objc dynamic var bs: String?
    @objc dynamic var latitude: String?
    @objc dynamic var longitude: String?
    override static func primaryKey() -> String? {
        return "id"
    }
}




//var description: String{
//class Adress{


