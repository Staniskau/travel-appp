//
//  DataBaseManager.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/29/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import RealmSwift


class DatabaseManager {
    
    
    func save<T> (_ objects: [T]) {
        let realm = try! Realm()
        try! realm.write {
            if let objects = objects as? [Object]{
            realm.add(objects, update: true)
            }
        }
    }
    func save<T: Object, S> (_ objects: [T], types: S) {
        let realm = try! Realm()
        try! realm.write {
                realm.add(objects, update: true)
        }
    }
    
    func getObjects<T: Object> (classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
    
    static var intance = DatabaseManager()
    func saveToDatabas(users: [RealmsUser]) {
       let realm = try! Realm()
        
        try! realm.write {
            realm.add(users, update: true)
        }
    }
    func saveToDatabase(travel: RealmTravel ) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(travel, update: true)
        }
        
    }
    func getUsersFromDatabase() -> [RealmsUser] {
        let realm = try! Realm()
        let usersResult = realm.objects(RealmsUser.self)
        var users: [RealmsUser] = []
        for realmUser in usersResult {
            users.append(realmUser)
        }
        return users
    }
 
    func getTravelFromDatabase() -> [RealmTravel] {
        let realm = try! Realm()
        let realmTravelResult = realm.objects(RealmTravel.self)
        
//        var travels: [RealmTravel] = []
//        for realmTravel in realmTravelResult {
//            let travel = RealmTravel()
//            travel.name = realmTravel.name
//            travel.desc = realmTravel.desc
//            travels.append(travel)
//        }
        return Array(realmTravelResult)
    }
    
    func getTravelsFromDAtaBase(withId id: String) -> RealmTravel? {
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    func updateTravel(_ travel: RealmTravel, withName name: String) {
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
    
    func addStop(_ stop: RealmStop, to travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            travel.stops.append(stop)
        }
        
    }
    
    func deleteTravels(_ travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(travel)
           
        }
    }

    func deleteStop(_ stop: RealmStop) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(stop)
        }
    }
    
    
}
//func getUsersFromDatabase() -> [RealmTravels] {
//    let realm = try! Realm()
//    let usersResult = realm.objects(RealmTravels.self)
//    var users: [RealmTravels] = []
//    for realmUser in usersResult {
//        users.append(realmTravels)
//    }
//    return travel
//}
