//
//  User.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class User: CustomStringConvertible {
    var id: Int = 0
    var name: String?
    var username: String?
    var email: String?
    var phone: String?
    var website: String?
    var adress: Adress?
    var company: Company?
    var description: String{
        return """
        id: \(id)
        name: \(name)
        username: \(username)
        email: \(email)
        phone: \(phone)
        website:\(website)
        street: \(adress?.street)
        suite: \(adress?.suite)
        city: \(adress?.city)
        zipcode: \(adress?.zipcode)
        latitude: \(adress?.geo?.latitude)
        longitude: \(adress?.geo?.longitude)
        name: \(company?.name)
        catchPhrase: \(company?.catchPhrase)
        bs: \(company?.bs)
        """
    }
//
//    func printAll() {
//        print("""
//            \n
//            User:
//            id - \(id)
//            name - \(name)
//            username - \(username)
//            email - \(email)
//            phone - \(phone)
//            website - \(website)
//
//            Adress:
//            street - \(adress?.street)
//            suite - \(adress?.suite)
//            city - \(adress?.city)
//            zipcode - \(adress?.zipcode)
//
//            Geo:
//            latitude - \(adress?.geo?.latitude)
//            longitude - \(adress?.geo?.longitude)
//
//            Company:
//            name - \(company?.name)
//            catchPhrase -\(company?.catchPhrase)
//            bs - \(company?.bs)
//            \n
//            """)
//    }
}
