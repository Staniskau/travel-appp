//
//  Transports.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/22/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation



enum TransportType: Int {
    case airplane, car, train, none
}
//enum TransportType: String {
//    case none = ""
//    case airplane = "✈️"
//    case car =  "🚙"
//    case train = "🚂"
//}

enum CurrencuType: String {
    case none = ""
    case dollars = "$"
    case evro = "€"
    case ruble = "₽"
}
