//
//  File.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    static var instance = ApiManager()
    private enum Constants {
        static let baseURl = "https://jsonplaceholder.typicode.com"
    }
    private enum EndPoints {
        static let users = "/users"
    }
    
    func getUsers(onComplete: @escaping ([User]) -> Void) {
        let urlString =  Constants.baseURl + EndPoints.users
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            print(respons)
            switch respons.result {
                
            case .success(let data):
                print(data)
                if let arrayUsers = data as? Array<Dictionary<String,Any>> {
                    var users: [User] = []
                    for userDict in arrayUsers {
                        let user = User()
                        user.id = userDict["id"] as? Int ?? 0 // Или if let
                        user.name = userDict["name"] as? String ?? "no data"
                        user.username = userDict["username"] as? String ?? "no data"
                        user.email = userDict["email"] as? String ?? "no data"
                        user.phone = userDict["phone"] as? String ?? "no data"
                        user.website = userDict["website"] as? String ?? "no data"
                        users.append(user)
                        if let addressDict = userDict["address"] as? Dictionary<String,Any> {
                            let address = Adress()
                            address.street = addressDict["street"] as? String ?? ""
                            address.suite = addressDict["suite"] as? String ?? ""
                            address.city = addressDict["city"] as? String ?? ""
                            address.zipcode = addressDict["zipcode"] as? String ?? ""
                            user.adress = address
                            if let geoDict = addressDict["geo"] as? Dictionary<String, Any> {
                                let geo = Geo()
                                geo.latitude = geoDict["lat"] as? String ?? "no data"
                                geo.longitude = geoDict["lng"] as? String ?? "no data"
                                user.adress?.geo = geo
                                if let companyDict = userDict["company"] as? Dictionary<String,Any> {
                                    let company = Company()
                                    company.name = companyDict["name"] as? String ?? "no data"
                                    company.catchPhrase = companyDict["catchPhrase"] as? String ?? "no data"
                                    company.bs = companyDict["bs"] as? String ?? "no data"
                                    user.company = company
                                }
                            }
                        }
                        
                        
                    }
                    
                    onComplete(users)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
}
