//
//  stop.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/19/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class Stop {
    var currencu: CurrencuType?
    var delegate: StopsListViewController?
    var name: String = ""
    var rating: Int = 1
    var geolocation: String = ""
    var spendMoney: String = ""
    var transportType: TransportType?
    var desc: String = ""
    
    func printAll() {
        print("""
            Название - \(name)
            Рейтинг - \(rating)
            Потратил - \(spendMoney)
            Геолокация - \(geolocation)
            Транспорт - \(transportType)
            Описание - \(desc)
            """)
    }

    
}
