//
//  TrinityViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class TrinityViewController: UIViewController {
    //MARK: - Outlet
    var name: String = ""
    
    @IBOutlet weak var textField: UITextField!
    
    //MARK: - Action
    
    @IBAction func ButtonClicked(_ sender: Any) {
       let trinityVC = UIViewController.getFromStoryBoard(withId: "TrinityTwoViewController")
        navigationController?.pushViewController(trinityVC!, animated: true)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.layer.cornerRadius = 10
        
    }
    
}
