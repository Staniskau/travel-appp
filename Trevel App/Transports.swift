//
//  Transports.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/22/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

enum TransportType {
    case airplane, car, train
}
