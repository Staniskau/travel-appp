//
//  CreateStopViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/10/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import MapKit



class CreateStopViewController: UIViewController {
    
    
    var stop  = Stop()
    var delegate: TripsViewController?
    // MARK: - Outlet
    @IBOutlet weak var labelMoney: UILabel!
    @IBOutlet weak var nameTextFild: UITextField!
    @IBOutlet weak var transportTypeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var ratingTextFild: UITextField!
    
    @IBOutlet weak var geolocationLabel: UILabel!
    @IBOutlet weak var descTextFild: UITextView!
    
    // MARK: - Action
    
    @IBAction func chooseCurrencyClicked(_ sender: Any) {
        let controllerFromStoryBord = UIViewController.getFromStoryBoard(withId: "SpendManeyViewController")
        //            as? SpendManeyViewController
        if let spendMoneyVC = controllerFromStoryBord as? SpendManeyViewController {
            spendMoneyVC.delegate = self
            navigationController?.pushViewController(controllerFromStoryBord!, animated: true)
        }
    }
    
    @IBAction func MapButtonClicked(_ sender: Any) {
        let minsk = MKPointAnnotation()
        minsk.coordinate = CLLocationCoordinate2D(latitude: 53.925, longitude: 27.508)
        let mosscow = MKPointAnnotation()
        mosscow.coordinate = CLLocationCoordinate2D(latitude: 55.688, longitude: 37.689)
       
        let mapVC = UIViewController.getFromStoryBoard(withId: "MapViewController") as! MapViewController
        mapVC.array = [minsk, mosscow]
        mapVC.delegate = self
        navigationController?.pushViewController(mapVC, animated: true)
    }

    
    @IBAction func ratingButtonClicked(_ sender: UIStepper) {
        var number = 0
        number = Int(sender.value)
        ratingTextFild.text = String(number)
    }
    
    
    @IBAction func segmentedControlClicked(_ sender: Any) {
        if transportTypeSegmentedControl.selectedSegmentIndex == 0 {
            stop.transportType = .airplane
        }else {
            if transportTypeSegmentedControl.selectedSegmentIndex == 1 {
                stop.transportType = .train
            } else {
                if transportTypeSegmentedControl.selectedSegmentIndex == 2 {
                    stop.transportType = .car
                }
            }
        }
    }

    
    
  
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        if let name = nameTextFild.text {
            stop.name = name
        }
        if let rating = ratingTextFild.text{
            stop.rating = rating
        }
        if let geolocationSite = geolocationLabel {
            let geolocation = Geolocation()
            geolocation.latitide = "53'222"
            geolocation.longitude = "27'44"
            stop.geolocation = geolocation
        }
        if let spendMoney = labelMoney.text{
            stop.spendMoney = spendMoney
        }
        if let desc = descTextFild.text {
            stop.desc = desc
        }
        
    }
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  

    func urerSpendMoney(_ moneyCout: String){
        print("Я потратил \(moneyCout)")
        labelMoney.text = moneyCout
    }
    
    func urerMap(_ geolocation: Geolocation){
        print("Я потратил \(geolocation)")
        stop.geolocation = geolocation
    }
    
}

extension CreateStopViewController: MapViewControllerDelegate{
    func mapControllerDidSelecPoint(_ point: MKPointAnnotation) {
     print("Найди в поиске эту строку,jона поможкт тебе найти лейбел  ")
    }
    
    
}
