//
//  HomeWorkcViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class HomeWorkcViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var labelText: UILabel!
    
    
    
    
    @IBAction func buttonClicked(_ sender: Any) {
        let controllerFromStoryboard = storyboard?.instantiateViewController(withIdentifier: "HomeWorkLabelViewController")
        if let labelVC = controllerFromStoryboard as? HomeWorkLabelViewController {
            labelVC.delegate = self
            navigationController?.pushViewController(labelVC, animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.layer.cornerRadius = 10
    }
    func backLabel (_ text: String) {
        labelText.text = text
    }
    
    
}
