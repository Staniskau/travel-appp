//
//  ForthViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/8/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import UIKit


class ForthViewController:UIViewController {
    
    let progress = Progress(totalUnitCount: 10)
    @IBAction func closeButtonClicket(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func startButton(_ sender: Any) {
        self.activityIndicator.startAnimating()
    }
    @IBAction func stopButton(_ sender: Any) {
        self.activityIndicator.stopAnimating()
    }
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBAction func startProgress(_ sender: Any) {
        progressView.progress = 0.0
        progress.completedUnitCount = 0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                return
            }
            self.progress.completedUnitCount += 1
            self.progressView.setProgress(Float(self.progress.fractionCompleted), animated: true)
            
            self.progressLabel.text = "\(Int(self.progress.fractionCompleted * 100)) %"
        }
    }
    
  
    @IBOutlet weak var pageControler: UIPageControl!
    
 
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    

    
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        var number = 0
        number = Int(sender.value)
        label.text = String(number)
    }
    
    
}

















