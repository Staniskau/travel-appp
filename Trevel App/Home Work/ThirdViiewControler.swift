//
//  ThirdViiewControler.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/8/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import UIKit

class ThirdViewControler:UIViewController {
    var name: String  = ""
    @IBAction func CloseButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var textFieldName: UITextField!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        name = textFieldName.text!
        labelName.text = "Hello \(name)"
    }
    
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func sliderValuechanged(_ sender: Any) {
        label.text = "\(slider.value)"
    }
    
    @IBOutlet weak var stateSwitch: UISwitch!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBAction func buttonClick(_ sender: Any) {
        if stateSwitch.isOn {
            textLabel.text = "The Switch is On"
        } else {
            textLabel.text = "The Switch is Off"
        }
        
    }
    
}
