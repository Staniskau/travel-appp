//
//  HomeWorkLabelViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class HomeWorkLabelViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var TextFildLabel: UITextField!
    
    var delegate: HomeWorkcViewController?
    
    
    @IBAction func backClicked(_ sender: Any) {
        if let text = TextFildLabel.text {
            delegate?.backLabel(text)
        }
        navigationController?.popViewController(animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = delegate?.textField.text
    }
}
    

