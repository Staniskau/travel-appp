//
//  adress.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation


class Adress{
    var suite: String?
    var street : String?
    var city: String?
    var zipcode: String?
    
    var geo: Geo?
    var company: Company?
}

class Geo{
    var latitude: String?
    var longitude: String?
}

class Company {
    
    var name: String = ""
    var catchPhrase: String = ""
    var bs: String = ""
    
}
