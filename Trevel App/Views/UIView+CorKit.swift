//
//  UIView+CorKit.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/12/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

extension UIView {
    
    var cornerRadius: CGFloat
}
