//
//  travelCellTable.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelSuptitleLabel: UILabel!

    @IBOutlet weak var starOne: UIImageView!
    
    @IBOutlet weak var starTwo: UIImageView!
    
    @IBOutlet weak var starTree: UIImageView!
    
    @IBOutlet weak var starFore: UIImageView!
    @IBOutlet weak var starfive: UIImageView!
    
    @IBOutlet weak var transtportView: UIView!
    

    
    
}
