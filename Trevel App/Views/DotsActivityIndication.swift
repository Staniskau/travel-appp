//
//  DotsActivityIndication.swift
//  Trevel App
//
//  Created by Stanislau Reut on 6/12/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

enum AnimationKeys {
    static let group = "scaleGroup"
}

enum AnimationsConstants {
    static let dotScale: CGFloat = 1.5
    static let scaleUPDuration: CFTimeInterval = 0.2
    static let scaleDonwDuration: CFTimeInterval = 0.2
    static let offset: CFTimeInterval = 0.1
    
    
}

@IBDesignable
class DotsActivityIndication: UIView {
    
    var dots: [UIView] = []
    
    @IBInspectable
    var dotsCount: Int = 3 {
        didSet {
            removeDots()
            configureDots()
            setNeedsLayout()
        }
        willSet{
            startAnimation()
        }
    }
    
    @IBInspectable
    var dotRadius: CGFloat = 10{
        didSet {
            for dot in dots {
                configureDotSize(dot)
            }
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotSpacing: CGFloat = 15
    
    override var tintColor: UIColor! {
        didSet{
            for dot in dots{
                configureDotColor(dot)
            }
            setNeedsLayout()
        }
    }
  
    
    
    
    
    //MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureDots()
//        startAnimation()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        configureDots()
//        startAnimation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let offset = (frame.size.width - (2 * dotRadius + dotSpacing) * CGFloat(dotsCount) - dotSpacing)/2
        for i in 0..<dots.count {
            let y = frame.size.height / 2 - dotRadius
            let x = offset + (2 * dotRadius + dotSpacing) * CGFloat(i)
            let dot = dots[i]
            dot.frame.origin = CGPoint.init(x: x, y: y)
        }
    }
    //MARK: - Private
    func configureDots() {
        
        for i in 0..<dotsCount {
            let dot = UIView()
            dot.backgroundColor = .red
            configureDotColor(dot)
            configureDotSize(dot)
            dots.append(dot)
            addSubview(dot)
        }
        startAnimation()
    }
    func removeDots() {
        for dot in dots {
            dot.removeFromSuperview()
        }
        dots.removeAll()
    }
    func configureDotSize(_ dot: UIView) {
        dot.frame = CGRect(x: 0, y: 0, width: dotRadius * 2, height: dotRadius * 2)
        dot.cornerRadius = dotRadius
    }
    
    func configureDotColor(_ dot: UIView) {
    dot.backgroundColor = tintColor
    }
    func scaleAnimation(delay: CFTimeInterval) -> CAAnimationGroup {
        let scaleUP = CABasicAnimation(keyPath: "transform.scale")
        scaleUP.duration = AnimationsConstants.scaleUPDuration
        scaleUP.fromValue = 1
        scaleUP.toValue = AnimationsConstants.dotScale
        scaleUP.beginTime = delay
        
        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.duration = AnimationsConstants.scaleUPDuration
        scaleDown.fromValue = 1
        scaleDown.toValue = AnimationsConstants.dotScale
        scaleDown.beginTime = delay + scaleUP.duration
        
        let group = CAAnimationGroup()
        group.animations = [scaleUP, scaleDown]
        group.repeatCount = .infinity
        group.duration = (AnimationsConstants.scaleDonwDuration + AnimationsConstants.scaleUPDuration) * Double(dots.count)
        
        return group
    }
    
    
    //MARK: - Public
    func startAnimation() {
        var offset: CFTimeInterval = 0
        for dot in dots {
            dot.layer.removeAnimation(forKey: AnimationKeys.group)
            let animationGoup = scaleAnimation(delay: offset)
            dot.layer.add(animationGoup, forKey: AnimationKeys.group)
            offset = offset + AnimationsConstants.offset
        }
    
    }
    
    func stopAnimation() {
        for dot in dots {
            dot.layer.removeAnimation(forKey: AnimationKeys.group)
        }
    }
}
