//
//  StopCell.swift
//  Trevel App
//
//  Created by Stanislau Reut on 5/3/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class StopCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var stopTitleLabel: UILabel!
    @IBOutlet weak var stopSubtitleLabel: UILabel!
    

    @IBOutlet weak var stopSpendMoney: UILabel!
    @IBOutlet weak var transportView: UIView!

    @IBOutlet var stopsCollection: [UIView]!
    
    
    
    @IBOutlet var viewStops: UIView!
    
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFoer: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    //    MARK: - Action

    
}
